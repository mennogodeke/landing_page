Rails.application.routes.draw do

  resources :users

  root 'users#new'


  get "pages/impressum", :as => 'impressum'

  get "pages/anfrag_sp", :as => 'anfrag_sp'

  get "pages/anfragen", :as => 'anfragen'

  get "pages/daten", :as => 'daten'

  get "pages/inbox", :as => 'inbox'

  get "users/success", :as => 'success'

  get "pages/spieler_search", :as => 'spieler_search'

  get "pages/kond", :as => 'kond'

  get "pages/match", :as => 'match'

  get "pages/match_sp", :as => 'match_sp'

  get "pages/mes", :as => 'mes'

  get "pages/mes2", :as => 'mes2'

  get "pages/passwort", :as => 'passwort'

  get "pages/prof_sp", :as => 'prof_sp'

  get "pages/profil_sp", :as => 'profil_sp'

  get "pages/profil_ver", :as => 'profil_ver'

  get "pages/soc", :as => 'soc'

  get "pages/sp_res", :as => 'sp_res'

  get "pages/thanks", :as => 'thanks'

  get "pages/ver_pr", :as => 'ver_pr'

  get "pages/ver_res", :as => 'ver_res'

  get "pages/verein_search", :as => 'verein_search'

  get "pages/vertrag", :as => 'vertrag'

  if Rails.env.production?
    get '400', :to => 'errors#already_exists'
    get '404', :to => 'errors#page_not_found'
    get '422', :to => 'errors#server_error'
    get '500', :to => 'errors#server_error'
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
