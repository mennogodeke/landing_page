class User < ActiveRecord::Base
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

  after_create :subscribe_user_to_mailing_list
  #after_create :send_welcome_email_to_user


  def send_welcome_email_to_user
    UserMailer.welcome_email(self).deliver_later
  end

  private

    def subscribe_user_to_mailing_list
      SubscribeUserToMailingListJob.perform_now(self)
    end
end
