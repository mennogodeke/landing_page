class SubscribeUserToMailingListJob < ActiveJob::Base
  queue_as :default
	
require 'dotenv'
Dotenv.load

  def perform(user)
    gb = Gibbon::Request.new(api_key: ENV["MAILCHIMP_API_KEY"])
    gb.lists(ENV["MAILCHIMP_LIST_ID"]).members.create(body: {email_address: user.email, status: "pending", :double_optin => true, :update_existing => false, :send_welcome => true})
  end
end
