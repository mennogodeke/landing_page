class UsersController < ApplicationController

  def show

  end

  def daten
    render template: "users/daten"
  end

  def impressum
    render template: "users/impressum"
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
    render "users/success"
    else
      render "errors/already_exists"
    end
  end

  def index
    @users = User.all
  end
  

  private

    def user_params
      params.require(:user).permit(:email)
    end

end
