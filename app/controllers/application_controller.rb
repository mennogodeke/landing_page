class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  http_basic_authenticate_with :name => "development", :password => "knoblauch99", if: :authenticate?

  private

    def authenticate?
      if ((controller_name == 'pages' && %w( daten impressum).include?(action_name)) || (controller_name == 'users'))
        false
      else
        true
      end
    end
end
