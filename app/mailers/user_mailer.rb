class UserMailer < ApplicationMailer
  default from: "info@soccertransfair.com"

  def welcome_email(user)
    @user = user
    mail(to: @user.email, subject: "Welcome to Soccertransfair!")
  end
end
